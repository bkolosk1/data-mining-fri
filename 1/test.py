import csv
f = open("eurovision-finals-1975-2019.csv", "rt", encoding="utf8")
first = True
cntfrom = set()
cntto = set()
cnt2pair = {}
cnt2pos = {}
cnt_app = {}
pos = 0
limit = 2018
cnt2app = {}
for l in csv.reader(f):
    if not(first) and int(l[0]) > 1903:
        #if l[2] == 'Slovakia' or l[3] =="Slovakia":
        #    continue
        #print(l)
        year = l[0]
        fromCnt = l[2]
        toCnt = l[3]
        points = int(l[4])
        #Solving name issue 
        if l[2] == "North Macedonia" or l[2]=="F.Y.R. Macedonia":
            fromCnt="Macedonia"
        if l[3] == "North Macedonia" or l[3]=="F.Y.R. Macedonia":
            toCnt = "Macedonia"
        if not(fromCnt in cnt2pair):
            cnt2pair[fromCnt] = []
        #Spicy
        if toCnt == 10:
            toCnt = 9
        if toCnt == 12:
            toCnt = 10
        if not(toCnt in cnt2app):   
            cnt2app[toCnt] = set()
        cnt2app[toCnt].add(year)
        cnt2pair[fromCnt].append((toCnt,points))
        cntto.add(toCnt)
        cntfrom.add(fromCnt)
    first = False
buildfrom = min(cntto,cntfrom)

#fix
cntapp = {}
for c in cnt2app:
    print(cnt2app)
    cntapp[c] = len(cnt2app[c])

for c in cntapp:
    print(cntapp[c])

for c in buildfrom:
    cnt2pos[c] = pos
    pos = pos + 1

print(cnt2pos)
votes = []
dist_mat = {}
for x in cnt2pair: 
    votes = [0] * (pos)
    for p_cnt in cnt2pair[x]:
        to_cnt,points = p_cnt
        votes[cnt2pos[to_cnt]] = points
    dist_mat[x]=votes
    votes.clear()

#print(dist_mat.keys())