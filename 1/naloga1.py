import csv
import math 
import numpy as np
"""Processing mostly uses sets to store unique countries, countries_appereances and other mutable data. After that the data is processed and contries enumerated.
Since the limit country is used 2007 and later as to prevent the backlog of ex-Socialist state and preserve the fluidness of European geo-Politics."""
def read_file(file_name):
    f = open("eurovision-finals-1975-2019.csv", "rt", encoding="utf8")
    first = True
    cntfrom = set()
    cntto = set()
    cnt2pair = {}
    cnt2pos = {}
    cnt_app = {}
    d_pos = 0
    limit = 2006
    for l in csv.reader(f):
        if not(first) and int(l[0]) > limit:
            #if l[2] == 'Slovakia' or l[3] =="Slovakia":
            #    continue
            #print(l)
            fromCnt = l[2]
            toCnt = l[3]
            points = int(l[4])
            #Solving name issue 
            if l[2] == "North Macedonia" or l[2]=="F.Y.R. Macedonia":
                fromCnt="Macedonia"
            if l[3] == "North Macedonia" or l[3]=="F.Y.R. Macedonia":
                toCnt = "Macedonia"
            if not(fromCnt in cnt2pair):
                cnt2pair[fromCnt] = []
            if not(toCnt in cnt_app):
                cnt_app[toCnt] = set()
            cnt_app[toCnt].add(l[0])
            cnt2pair[fromCnt].append((toCnt,points))
            cntto.add(toCnt)
            cntfrom.add(fromCnt)
        first = False

    buildfrom = cntto
    
    ret_size = len(buildfrom)
    dist_mat = {}    
    for c in buildfrom:
        dist_mat[c] = [0] * ret_size
    
    cnt2app = {}
    for c in cnt_app:
        cnt2app[c] = len(cnt_app[c])

    for c in buildfrom:
        cnt2pos[c] = d_pos
        d_pos = d_pos + 1

    for x in cnt2pair: 
        if x in cntto:
            for p_cnt in cnt2pair[x]:
                to_cnt,points = p_cnt
                dist_mat[x][cnt2pos[to_cnt]] += points/cnt2app[to_cnt]
                #dist_mat[to_cnt][cnt2pos[x]] += points/cnt2app[to_cnt]

    return dist_mat

class HierarchicalClustering:
    """Initialize the clustering"""
    def __init__(self, data):
        self.data = data
        # self.clusters stores current clustering. It starts as a list of lists
        # of single elements, but then evolves into clusterings of the type
        # [[["Albert"], [["Branka"], ["Cene"]]], [["Nika"], ["Polona"]]]
        self.clusters = [[name] for name in self.data.keys()]
      
    """
        Distance between two rows.
        Implement either Euclidean or Manhattan distance.
        Example call: self.row_distance("Polona", "Rajko")
    """
    def row_distance(self, r1, r2):
        d_dist = 0
        for d_x,d_y in zip(self.data[r1],self.data[r2]):
            d_dist = d_dist + (d_x-d_y)*(d_x-d_y)
        return math.sqrt(d_dist)       
    """
        Helper method designed for flattening the list of clusters. Since after merging I create sub lists of the list
    """
    def flatten(self,l_list):
        if l_list == []:
            return l_list
        if type(l_list) is str:
            return [l_list]
        else:
            return self.flatten(l_list[0]) + self.flatten(l_list[1:])
    """
        Compute distance between two clusters.
        Implement either single, complete, or average linkage.
        Example call: self.cluster_distance(
            [[["Albert"], ["Branka"]], ["Cene"]],
            [["Nika"], ["Polona"]])
        Cluster distance based on single linkage.
        def cluster_distance(self, c1, c2):
            l_c1_flattened = self.flatten(c1)
            l_c2_flattened = self.flatten(c2)
            d_dist_min = np.inf
            for x_i in l_c1_flattened:
                for x_j in l_c2_flattened:
                    d_dist_min = min(d_dist_min,self.row_distance(x_i, x_j))
            return d_dist_min
        Cluster distance based on complete linkage.
    """
    def cluster_distance(self, c1, c2):
        l_c1_flattened = self.flatten(c1)
        l_c2_flattened = self.flatten(c2)
        d_dist_max = 0
        for x_i in l_c1_flattened:
            for x_j in l_c2_flattened:
                d_dist_max = max(d_dist_max,self.row_distance(x_i, x_j))
        return d_dist_max
    
    """
        Find a pair of closest clusters and returns the pair of clusters and
        their distance.

        Example call: self.closest_clusters(self.clusters)
    """        
    def closest_clusters(self):
        p_first_clust = ""
        p_second_clust = ""
        d_dist_min = np.inf        
        for x in self.clusters:
            for y in self.clusters:
                d_x_y = self.cluster_distance(x,y)
                if x!= y and d_x_y < d_dist_min:
                    d_dist_min,p_first_clust,p_second_clust = d_x_y,x,y
        return d_dist_min,(p_first_clust,p_second_clust)
    """
        Given the data in self.data, performs hierarchical clustering.
        Can use a while loop, iteratively modify self.clusters and store
        information on which clusters were merged and what was the distance.
        Store this later information into a suitable structure to be used
        for plotting of the hierarchical clustering.
    """        
    def run(self):
        # store information about merging - kind of 'pointers' for further needs
        while (len(self.clusters) > 2):
            a, (x, y) = self.closest_clusters()
            self.clusters.remove(x)
            self.clusters.remove(y)
            self.clusters.append([x] + [y])
    """
        Method for printing the dendrogram, the idea is to print it in a Depth First Search manner, since the list is made of merging two childs (of type list)
        the base case is it is a single cluster then it is of type(string). It is provided with two additional parameters, the string that inhabits the empty space
        and the string that exibits concatanation part.
        Example call: self.plot_tree_util(self.clusters,0,"---|","   ") 
    """
    def plot_tree_util(self,l_clusters,d_depth,s_printT,s_print_):
        if len(l_clusters) == 1:
            s_cluster_name = l_clusters[0]
            for i in range(0,d_depth):
                print(s_print_,end='')
            print(s_cluster_name)            
        else:
            l_left_children = l_clusters[0]
            l_right_children = l_clusters[1]
            d_new_depth = 1 + d_depth
            self.plot_tree_util(l_left_children,d_new_depth,s_printT,s_print_)
            for i in range(0,d_depth):
                print(s_print_,end='')
            print(s_printT)
            self.plot_tree_util(l_right_children,d_new_depth,s_printT,s_print_)
    """
        Use cluster information to plot an ASCII representation of the cluster
        tree.
    """
    def plot_tree(self):      
        self.plot_tree_util(self.clusters,0,"---|","    ")

if __name__ == "__main__":
    DATA_FILE = "eurovision-final.csv"
    hc = HierarchicalClustering(read_file(DATA_FILE))
    hc.run()
    hc.plot_tree()