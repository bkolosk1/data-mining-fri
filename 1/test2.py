import csv 

f = open("eurovision-finals-1975-2019.csv", "rt", encoding="utf8")

isciIndeks = []
drzave = {}
drzaveArray = []

firstRow = f.readline()
inputZapis = []

stevecSendDrzav = 0
stevecVrsticInputa = 0
stevecIndeks = 0

for row in csv.reader( f ):
    if ( row[2] == 'F.Y.R. Macedonia' ):
        row[2] = 'North Macedonia'
    if ( row[3] == 'F.Y.R. Macedonia' ):
        row[3] = 'North Macedonia'
    row[4] = int(row[4])
    if ( row[4] == 10 ):
        row[4] = 9
    if ( row[4] == 12 ):
        row[4] = 10
    inputZapis.append([ row[0], row[1], row[2], row[3], row[4] ] )
    stevecVrsticInputa += 1

    if ( [ row[0], row[1], row[3] ] not in isciIndeks ):
        if ( row[3] != 'Serbia & Montenegro' and row[3] != 'Yugoslavia' ):
            isciIndeks.append( [ row[0], row[1], row[3] ] )
            stevecIndeks += 1
            
for row in inputZapis:
    if row[2] not in drzave:
        if ( row[2] != "Serbia & Montenegro" and row[2] != 'Yugoslavia' ):
            drzave[ row[2] ] = [None]*stevecIndeks
            stevecSendDrzav += 1
            drzaveArray.append( row[2] )

for row in inputZapis:
    if row[2] in drzave:
        if ( [ row[0], row[1], row[3] ] in isciIndeks):
            ind = isciIndeks.index( [ row[0], row[1], row[3] ] )
            drzave[ row[2] ][ind] = row[4]



