import linear
import numpy as np
import os
from unidecode import unidecode
from sklearn.preprocessing import PolynomialFeatures
import csv
import datetime
rainWhole = [[0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1],[0,0,1,1,1,1,1,0,0,0,1,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0],[1,0,0,1,1,1,1,1,0,0,1,0,1,1,1,1,0,0,1,1,1,1,1,1,0,0,0,0,0,0],[0,1,0,0,0,1,1,0,0,0,1,0,0,0,0,1,0,0,0,0,1,1,0,1,0,0,0,0,0,0,1],[1,1,0,0,0,0,0,0,1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0],[0,0,0,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,1,0,0,1,1,0,0,0,1],[1,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,1,0,1,1,0,0,1],[1,1,0,0,0,0,0,0,1,1,0,1,1,0,1,1,0,0,0,0,0,0,0,0,1,0,1,1,1,0,1],[1,0,0,1,1,0,0,0,0,0,1,1,1,0,0,1,0,0,0,1,1,0,0,0,0,1,1,1,1,1],[0,1,0,1,0,0,0,1,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,1]]
holidays = [(1,1),(1,2),(2,8),(4,8),(4,9),(4,27),(5,1),(5,2),(5,27),(6,25),(8,15),(10,31),(11,1),(12,25),(12,26)]
FORMAT = "%Y-%m-%d %H:%M:%S.%f"
def parsedate(x):
    if not isinstance(x, datetime.datetime):
        x = datetime.datetime.strptime(x, FORMAT)
    return x

def tsdiff(x, y):
    return (parsedate(x) - parsedate(y)).total_seconds()

def tsadd(x, seconds):
    d = datetime.timedelta(seconds=seconds)
    nd = parsedate(x) + d
    return nd.strftime(FORMAT)
res = 0
calc = 0
Y = []
outTime = []
models = {}
dataLines = {}
def readAll():
    f = open("train.csv", "rt", encoding="utf8")
    global dataLines
    first = False
    lineBegan = False
    lastLine = -1
    for l in csv.reader(f):
        if first:   
            parts = l[0].split("\t")
            route = parts[2]
            if not route in dataLines:
                dataLines[route] = []
            dataLines[route].append(parts)
        first = True

def buildLines():
    global dataLines
    for route in dataLines:
        #print("BUILDING MODEL: " + route)
        X,Y = buildModel(route)
        vanillTest(route,X,Y)
        lr = linear.LinearLearner(lambda_=0.001)
        models[route] = lr(X,Y)
        #print(models[route].th)
    dataLines.clear()

def vanillTest(route,X,Y):
    global res
    global calc
    lamba = 1
    lr = linear.LinearLearner(lambda_=lamba)
    y_ = test_cv(lr,X,Y,k=5)
    res += MAE(Y,y_)
    calc = calc + 1
    print("ROUTE: "+str(route)+" MAE: "+str(MAE(Y,y_))+" MSE: "+str(MSE(Y,y_)))

def fixBus(bus):
    bus = bus.split()
    bus = bus[1].split('-')
    bus = bus[1]
    bus = bus.replace('*','')
    return int(bus)
directionMap = {}
backDirection = {}
def buildModel(route_):
    Y = []
    busReg = []
    rushLow = []
    rushHigh = []
    weekmap = []
    didRain = []
    hourMap = []
    monthMap = []
    garageMap = []
    directionCol = []
    backCol = []
    lineCount = 0
    global directionMap
    global backDirection
    global dataLines
    for parts in dataLines[route_]:
        #FIX BUS
        #Driver
        driver = parts[1]
        route = parts[2]
        direction = parts[3]
        start = parts[4]
        departureTime = parts[6]
        finishTime = parts[8]
        dayMap = parsedate(departureTime)
        tmpMap = [0]*7    
        #Handle separate direction 
        if not route in directionMap:
            directionMap[route] = start
        if directionMap[route] == start:
            directionCol.append(1)
        else:
            directionCol.append(0)
        #HANDLE BACK
        if not route in backDirection:
            backDirection[route] = start
        if backDirection[route] == start:
            backCol.append(1)
        else:
            backCol.append(0)
        #Handle bus going to garage
        if direction.find('GARAŽA'):
            garageMap.append(1)
        else:
            garageMap.append(0)
        #Handle holidays
        package = (dayMap.month,dayMap.day)
        if package in holidays:
            tmpMap[6] = 1
        else:
            tmpMap[dayMap.weekday()] = 1
        rushLow.append(0)
        rushHigh.append(0)
        if dayMap.weekday() in range(5) and dayMap.hour >= 6 and dayMap.hour <= 8 and tmpMap[6]==0 and tmpMap[5] == 0:
            rushLow[lineCount] = 1
        if dayMap.weekday() in range(5) and dayMap.hour >= 14 and dayMap.hour <= 17 and tmpMap[6]==0 and tmpMap[5] == 0:
            rushHigh[lineCount] = 1
        weekmap.append(tmpMap)
        didRain.append(rainWhole[dayMap.month-1][dayMap.day-1])
        #HANDLE HOUR OF DAY
        currDay = [0]*24
        currDay[dayMap.hour] = 1
        hourMap.append(currDay)
        #HANDLE MONTH
        tmpMonth = [0]*12
        tmpMonth[dayMap.month-1] = 1
        monthMap.append(tmpMonth)       
        #Y
        dt = tsdiff(finishTime,departureTime)
        Y.append(dt)
        lineCount = lineCount + 1    
    #BUILD
    garageMap = np.array((garageMap)).reshape((lineCount,1))
    weekmap = np.array((weekmap))
    monthMap = np.array((monthMap))
    didRain = np.array((didRain)).reshape((lineCount,1))
    rushLow = np.array((rushLow)).reshape((lineCount,1))
    rushHigh = np.array((rushHigh)).reshape((lineCount,1))    
    directionCol = np.array((directionCol)).reshape((lineCount,1))
    backCol = np.array((backCol)).reshape((lineCount,1))
    #END GAME
    X = np.hstack((backCol,directionCol,garageMap,weekmap,didRain,rushHigh,rushLow,monthMap,hourMap))   
    Y = np.array((Y))
    print(X.shape)
    return X,Y

def solveInit():
    fp = open("test.csv", "rt", encoding="utf8")
    f = open("solved.txt","w+")
    first = True
    j = 0
    for l in csv.reader(fp):
        if not first:
            parts = l[0].split("\t")
            C,outTime = solveLine(l)
            route = parts[2]
            model = models[route]
            h = model(C)   
            f.write("%s\n" % tsadd(parsedate(outTime),h))
            j = j + 1
        first = False
    fp.close()
    f.close()

def solveLine(l):   
    global directionMap
    parts = l[0].split("\t")
    bus = fixBus(parts[0])
    driver = parts[1]
    route = parts[2]
    start = parts[4]
    finish = parts[5]
    direction = parts[3]
    departureTime = parts[6]
    dayMap = parsedate(departureTime)
    tmpMap = [0]*7
    package = (dayMap.month,dayMap.day)
    line = []       
    #START
    if directionMap[route] == start:
        line.append(1)
    else:
        line.append(0)
    if backDirection[route] == finish:
        line.append(1)
    else:
        line.append(0)
    #GARAGEE
    if direction.find('GARAŽA'):
        line.append(1)
    else:
        line.append(0)  
    #HOLIDAY
    if package in holidays:
        tmpMap[6] = 1
    else:
        tmpMap[dayMap.weekday()] = 1
    line = line + tmpMap
    #RUSH
    rushLow = 0
    if dayMap.weekday() in range(5) and dayMap.hour >= 6 and dayMap.hour <= 8 and tmpMap[6]==0 and tmpMap[5] == 0:
        rushLow = 1
    line.append(rushLow)
    rushHigh = 0
    if dayMap.weekday() in range(5) and dayMap.hour >= 14 and dayMap.hour <= 17 and tmpMap[6]==0 and tmpMap[5] == 0:
        rushHigh = 1
    line.append(rushHigh)
    #RAIN DATA
    didRain = rainWhole[dayMap.month-1][dayMap.day-1]
    line.append(didRain)
    #HANDLE HOUR OF DAY
    currDay = [0]*24
    currDay[dayMap.hour] = 1
    #HANDLE MONTH
    tmpMonth = [0]*12
    tmpMonth[dayMap.month-1] = 1
    monthMap = tmpMonth   
    line = line + monthMap
    line = line + currDay
    #Y
    outTime = departureTime
    #BUILD
    X = np.array(line)
    return X,outTime

def MAE(calculated, expected):
    if len(calculated)!=len(expected) or len(calculated) == 0 or len(expected) == 0:
        return 0
    return np.sum(np.abs(np.subtract(calculated,expected)))/len(calculated)

def MSE(calculated, expected):
    if len(calculated)!=len(expected) or len(calculated) == 0 or len(expected) == 0:
        return 0
    return np.sum(np.square(np.subtract(calculated,expected)))/len(calculated)

def test_cv(learner, X, y, k=5):
    m, n = X.shape
    shuffle = np.random.permutation(m)
    folds = np.array_split(shuffle,k)
    out = np.zeros((m))
    iter_ = 0
    for i in range(k):
        idxs = list(set(shuffle) - set(folds[i]))
        trainX = X[idxs,:]
        trainY = y[idxs]
        testX = X[folds[i],:]
        classify = learner(trainX,trainY)
        for j in range(len(folds[i])):
            out[shuffle[iter_]] = classify(testX[j])
            iter_ = iter_ + 1
    return out

if __name__ == "__main__":
    readAll()
    buildLines()
    solveInit()
    print("AVG "+str(res/calc))

    