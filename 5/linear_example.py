import linear
import numpy as np
import os
from unidecode import unidecode
import matplotlib.pyplot as plot
import math
import operator
import csv
import datetime
rainWhole = [[0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1],[0,0,1,1,1,1,1,0,0,0,1,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0],[1,0,0,1,1,1,1,1,0,0,1,0,1,1,1,1,0,0,1,1,1,1,1,1,0,0,0,0,0,0],[0,1,0,0,0,1,1,0,0,0,1,0,0,0,0,1,0,0,0,0,1,1,0,1,0,0,0,0,0,0,1],[1,1,0,0,0,0,0,0,1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0],[0,0,0,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,1,0,0,1,1,0,0,0,1],[1,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,1,0,1,1,0,0,1],[1,1,0,0,0,0,0,0,1,1,0,1,1,0,1,1,0,0,0,0,0,0,0,0,1,0,1,1,1,0,1],[1,0,0,1,1,0,0,0,0,0,1,1,1,0,0,1,0,0,0,1,1,0,0,0,0,1,1,1,1,1],[0,1,0,1,0,0,0,1,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,1]]
FORMAT = "%Y-%m-%d %H:%M:%S.%f"
def parsedate(x):
    if not isinstance(x, datetime.datetime):
        x = datetime.datetime.strptime(x, FORMAT)
    return x

def tsdiff(x, y):
    return (parsedate(x) - parsedate(y)).total_seconds()

def tsadd(x, seconds):
    d = datetime.timedelta(seconds=seconds)
    nd = parsedate(x) + d
    return nd.strftime(FORMAT)
  
Y = []

"""
    if __name__ == "__main__":
    testd1 = "2012-01-01 23:32:38.000"
    testd2 = "2012-12-01 03:33:38.000"
    
    testd1 = datetime.datetime.strptime(testd1, FORMAT)
    print(testd2)
    for i in range(23000):
        a = tsdiff(testd1, testd2)
        b = tsadd(testd1, -122)
        print(a)

    X = np.array([[1,3],
                     [2,2],
                     [3,3]])

    y = np.array([10,11,12])

    lr = linear.LinearLearner(lambda_=1.)
    napovednik = lr(X,y)

    print ("Koeficienti", napovednik.th) #prvi je konstanten faktor

    nov_primer = np.array([2,11])
    print ("Napoved", napovednik(nov_primer))
"""
outTime = []
def read():
    f = open("train_pred.csv", "rt", encoding="utf8")
    #CHECK
    first = True
    foundStart = False
    #LOW
    rushLow = [] 
    rushHigh = []
    #COUNTS DRIVERS
    driversIdx = {}
    driverCount = 0
    #MAPS DRIVERS TO IDX'S
    drivers = {}
    #COUNTS BUSID
    busId = {}
    #COUNTS AND MAPS BUS REGISTRATIONS
    busCount = 0
    busses = {}
    #TRAVEL TIME
    #Week:
    weekmap = []
    didRain = []
    #HOUR
    hourMap = []
    monthMap = []
    #DIR
    directionMap = []
    global Y
    lineCount = 0
    #GARAGE
    garageMap = []
    for l in csv.reader(f):
        if not first:
            parts = l[0].split("\t")
            #PARTS
            bus = parts[0]
            driver = parts[1]
            route = parts[2]
            direction = parts[3]
            departureTime = parts[6]
            finishTime = parts[8]
            #TRAVEL TIME:
            dayMap = parsedate(departureTime)
            tmpMap = [0]*7
            if direction.find('GARAŽA'):
                garageMap.append(1)
            else:
                garageMap.append(0)
            #DIRECTION
            if direction == "VRHOVCI":
                directionMap.append(1)
            else:
                directionMap.append(0)
            if dayMap.day == 25 or dayMap.day == 26:
                tmpMap[6] = 1
            else:
                tmpMap[dayMap.weekday()] = 1
            rushLow.append(0)
            rushHigh.append(0)
            if dayMap.weekday() in range(5) and dayMap.hour >= 6 and dayMap.hour <= 8 and dayMap.day != 25 and dayMap.day != 26:
                rushLow[lineCount] = 1
            if dayMap.weekday() in range(5) and dayMap.hour >= 14 and dayMap.hour <= 17 and dayMap.day != 25 and dayMap.day != 26:
                rushHigh[lineCount] = 1
            weekmap.append(tmpMap)
            dt = tsdiff(finishTime,departureTime)
            didRain.append(rainWhole[dayMap.month-1][dayMap.day-1])
            currDay = [0]*24
            currDay[dayMap.hour] = 1
            hourMap.append(currDay)
            tmpMnt = [0]*12
            tmpMnt[dayMap.month-1] = 1
            monthMap.append(tmpMnt)
            #Y
            Y.append(dt)
            lineCount = lineCount + 1      
        first = False
    #HANDLE WEEK
    hourMap = np.array((hourMap))
    monthMap = np.array((monthMap))
    weekmap = np.array((weekmap))
    didRain = np.array((didRain)).reshape((lineCount,1))
    rushLow = np.array((rushLow)).reshape((lineCount,1))
    rushHigh = np.array((rushHigh)).reshape((lineCount,1))    
    directionMap = np.array((directionMap)).reshape((lineCount,1))
    garageMap = np.array((garageMap)).reshape((lineCount,1))
    #END GAME
    X = np.hstack((garageMap,hourMap,monthMap,directionMap,weekmap,didRain,rushHigh,rushLow))
    Y = np.array((Y))
    return X
outTime = []
def solve():
    f = open("test_pred.csv", "rt", encoding="utf8")
    first = True
    #LOW
    rushLow = [] 
    rushHigh = []
    #COUNTS DRIVERS
    driversIdx = {}
    driverCount = 0
    #MAPS DRIVERS TO IDX'S
    drivers = {}
    #COUNTS BUSID
    busId = {}
    #COUNTS AND MAPS BUS REGISTRATIONS
    busCount = 0
    busses = {}
    #TRAVEL TIME
    global outTime
    #Week:
    weekmap = []
    didRain = []
    #DIR
    directionMap = []
    lineCount = 0
    hourMap = []
    garageMap = []
    monthMap = []
    for l in csv.reader(f):
        if not first:
            parts = l[0].split("\t")
            #PARTS
            bus = parts[0]
            driver = parts[1]
            route = parts[2]
            direction = parts[3]
            departureTime = parts[6]
            outTime.append(departureTime)
            if direction.find('GARAŽA'):
                garageMap.append(1)
            else:
                garageMap.append(0)
            #DIRECTION
            if direction == "VRHOVCI":
                directionMap.append(1)
            else:
                directionMap.append(0)
            #TRAVEL TIME:
            dayMap = parsedate(departureTime)
            tmpMap = [0]*7
            if dayMap.day == 25 or dayMap.day == 26:
                tmpMap[6] = 1
            else:
                tmpMap[dayMap.weekday()] = 1
            rushLow.append(0)
            rushHigh.append(0)
            if dayMap.weekday() in range(5) and dayMap.hour >= 6 and dayMap.hour <= 8 and dayMap.day != 25 and dayMap.day != 26:
                rushLow[lineCount] = 1
            if dayMap.weekday() in range(5) and dayMap.hour >= 14 and dayMap.hour <= 17 and dayMap.day != 25 and dayMap.day != 26:
                rushHigh[lineCount] = 1
            weekmap.append(tmpMap)
            didRain.append(rainWhole[dayMap.month-1][dayMap.day-1])
            currDay = [0]*24
            currDay[dayMap.hour] = 1
            hourMap.append(currDay)
            tmpMnt = [0]*12
            tmpMnt[dayMap.month-1] = 1
            monthMap.append(tmpMnt)
            lineCount = lineCount + 1      
        first = False
    hourMap = np.array((hourMap))
    monthMap = np.array((monthMap))
    weekmap = np.array((weekmap))
    didRain = np.array((didRain)).reshape((lineCount,1))
    rushLow = np.array((rushLow)).reshape((lineCount,1))
    rushHigh = np.array((rushHigh)).reshape((lineCount,1))    
    directionMap = np.array((directionMap)).reshape((lineCount,1))
    garageMap = np.array((garageMap)).reshape((lineCount,1))
    #END GAME
    X = np.hstack((garageMap,hourMap,monthMap,directionMap,weekmap,didRain,rushHigh,rushLow))
    return X

def MAE(calculated, expected):
    if len(calculated)!=len(expected) or len(calculated) == 0 or len(expected) == 0:
        return 0
    return np.sum(np.abs(np.subtract(calculated,expected)))/len(calculated)

def MSE(calculated, expected):
    if len(calculated)!=len(expected) or len(calculated) == 0 or len(expected) == 0:
        return 0
    return np.sum(np.square(np.subtract(calculated,expected)))/len(calculated)
def test_cv(learner, X, y, k=5):
    m, n = X.shape
    shuffle = np.random.permutation(m)
    folds = np.array_split(shuffle,k)
    out = np.zeros((m))
    iter_ = 0
    for i in range(k):
        idxs = list(set(shuffle) - set(folds[i]))
        trainX = X[idxs,:]
        trainY = y[idxs]
        testX = X[folds[i],:]
        classify = learner(trainX,trainY)
        for j in range(len(folds[i])):
            out[shuffle[iter_]] = classify(testX[j])
            iter_ = iter_ + 1
    return out
if __name__ == "__main__":
    X = read()
    C = solve()
    j = 0
    print(X.shape)
    lamba = 10    
    for i in range(10):
        lamba = lamba/10
        lr = linear.LinearLearner(lambda_=lamba)
        y_ = test_cv(lr,X,Y,k=5)
        print("MAE: "+str(MAE(Y,y_))+" MSE: "+str(MSE(Y,y_)))
    lr = linear.LinearLearner(lambda_= 1.0)
    napovednik = lr(X,Y)
    print ("Koeficienti", napovednik.th) #prvi je konstanten faktor
    for i in C:
        h = napovednik(i)
        g = tsadd(parsedate(outTime[j]),h)
        print (g)
        j = j  + 1

