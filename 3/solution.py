import numpy as np
import os
from unidecode import unidecode
import matplotlib.pyplot as plot
import math
import operator
color_map = {'Romanian':'yo','Italian':'yo','Icelandic':'co','Norwegian':'co','English' : 'go','French' : 'go','German' : 'go',
        'Korean':'mo','Chinese' : 'mo','Japanese' : 'mo','Macedonian' : 'ko','Portuguese' : 'yo','Polish' : 'ko',
        'Russian':'ko','Czech':'ko','Slovenian' : 'ko','Spanish' : 'yo','Bosnian' : 'ko','Serbian' : 'ko','Swedish' : 'co'
}
error = np.finfo(np.float32).eps
def prepare_data_matrix():
    """
    Return data in a matrix (2D numpy array), where each row contains triplets
    for a language. Columns should be the 100 most common triplets
    according to the idf measure.
    """
    # create matrix X and list of languages
    # ...
    name_map = {'rum':'Romanian','itn':'Italian','ice':'Icelandic','nrn':'Norwegian','eng' : 'English','frn' : 'French','ger' : 'German',
        'kkn':'Korean','chn' : 'Chinese','jpn' : 'Japanese','mkj' : 'Macedonian','por' : 'Portuguese','pql' : 'Polish',
        'rus' : 'Russian','czc':'Czech','slv' : 'Slovenian','spn' : 'Spanish','src1' : 'Bosnian','src3' : 'Serbian','swd' : 'Swedish'}
    lang_triplets = {}
    name_list = list(name_map.keys())
    #READ DATA
    all_words = set()
    N = len(name_map)
    for x in name_map.keys():
        file_idx = x
        path = ".\\ready\\"+x+".txt"
        words = unidecode(open(path , "rt", encoding="utf8").read()).lower().split()    
        lang_triplets[file_idx] = {}
        unique_words = set()
        for word in words:
            for i in range(len(word)):
                tmp_str = word[i : i + 3]
                if not(tmp_str in lang_triplets[file_idx]):
                    lang_triplets[file_idx][tmp_str] = 0
                    unique_words.add(tmp_str)
                lang_triplets[file_idx][tmp_str] = lang_triplets[file_idx][tmp_str] + 1       
    final_words = {}
    for x in unique_words:
        final_words[x] = 0
        for y in name_list:
            if x in lang_triplets[y]:
                final_words[x] = final_words[x] + 1
        final_words[x] = -math.log(N/final_words[x])
    final_words = sorted(final_words.items(), key=operator.itemgetter(1), reverse=True)
    #CAST DATA TO NP MATRIX
    X = np.zeros((20,100))
    for x in range(N):
        for i in range(100):
            if (final_words[i][0] in lang_triplets[name_list[x]]):
                X[x][i] = lang_triplets[name_list[x]][final_words[i][0]]
    languages = list(name_map.values())
    return X, languages

"""
    Centers sampled data with respect to mean and standard deviation
    Calculates sampled variance = sum(X-X')/(N-1)
"""
def sampled_variance(X):
    N, M = X.shape
    mean_X = X.mean(axis = 0)
    centerd_X = (X-mean_X).T.dot(X-mean_X) / (N - 1)
    return centerd_X
"""
    Compute the eigenvector with the greatest eigenvalue
    of the covariance matrix of X (a numpy array).

    Return two values:
    - the eigenvector (1D numpy array) and
    - the corresponding eigenvalue (a float)
"""
def power_iteration(X):

    #Find dimensions of matrix
    n, m = X.shape
    #Sampled variance / same as np.cov for matrix X.T
    X = np.cov(X.T)
    #Generate random vector that will be used for power_itteration(=result)
    eig_vect = np.random.randn(m)
    eig_vect /= np.linalg.norm(eig_vect)
    #Stores eig val
    eig_val = -1
    while True:
        new_eig = X.dot(eig_vect)
        new_eig = new_eig / np.linalg.norm(new_eig)
        if np.linalg.norm(new_eig - eig_vect) <= error:
            break
        eig_vect = new_eig
    eig_val = X.dot(eig_vect)[0] /eig_vect[0]
    return eig_vect, eig_val

"""
Compute first two eigenvectors and eigenvalues with the power iteration method.
This function should use the power_iteration function internally.
Return two values:
- the two eigenvectors (2D numpy array, each eigenvector in a row) and
"""
def power_iteration_two_components(X):
    #Find firs eigenvector and value
    eig_vect_first, eig_val_first = power_iteration(X)
    #Project Data X to eigenvectors
    proj = project_to_eigenvectors(X, np.array([eig_vect_first]))
    X_projected = X - proj * eig_vect_first
    #Find the second eigenvector
    eig_vect_second,eig_val_second = power_iteration(X_projected)
    #Give eigenvectors together
    eig_vectors = np.array([eig_vect_first,eig_vect_second])
    #Give eigenvalues togetger
    eig_vals = np.array([eig_val_first, eig_val_second])
    return eig_vectors, eig_vals

"""
Project matrix X onto the space defined by eigenvectors.
The output array should have as many rows as X and as many columns as there
are vectors.
"""
def project_to_eigenvectors(X, vecs):
    #Center by mean
    X_mean = X.mean(axis = 0)
    centered = X-X_mean
    #Project the matrix X to vectors vecs
    projected_X = centered.dot(vecs.T)
    return projected_X

"""
Total variance of the data matrix X. You will need to use for
to compute the explained variance ratio.
"""
def total_variance(X):
    return np.var(X, axis=0, ddof=1).sum()

"""
    Compute explained variance ratio.
"""  
def explained_variance_ratio(X, eigenvectors, eigenvalues):
    #Sampled variance / same as np.cov for matrix X.T
    X_centered =  sampled_variance(X)
    #Sum of all eigenvaluescd ,,
    X_trace = np.trace(X_centered)
    #Sum of given eigenvalues
    eigenvalues_sum = eigenvalues.sum()
    explained_variance = eigenvalues_sum/X_trace
    return explained_variance


if __name__ == "__main__":
    # prepare the data matrix
    X, languages = prepare_data_matrix()
    N = len(languages)
    # PCA
    # ...
    eig_vect,eig_vals = power_iteration_two_components(X) 
    projections = project_to_eigenvectors(X, eig_vect)    
    # plotting
    # ... 
    plot.title('Explained variance: %.3f' % explained_variance_ratio(X, eig_vect, eig_vals))
    x_plot = []
    y_plot = []
    for i in range(N):
        x_plot.append(projections[i,0])
           
        y_plot.append(projections[i,1])
    plot.scatter(x_plot, y_plot)
    #Additional color is added as their presumed cluster based on their language group, 
    #variable fancy = true prints colors while false does not print
    #for more informative analysis
    fancy = True    
    for i in range(N):
        name = languages[i]
        if fancy:
            color = color_map[name]
            plot.plot(x_plot[i],y_plot[i],color)
        plot.annotate(name, (x_plot[i],y_plot[i]))
    plot.show()
    x_plot = []
    y_plot = []