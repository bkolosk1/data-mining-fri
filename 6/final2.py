import numpy as np
import os
from unidecode import unidecode
import csv

user2song = {}
queries = []
usw = list()
songs = set()
users = set()
userCount = 0
songCount = 0
userIdx = {}
songIdx = {}
currentSong = {}
def parseLineTrain(l):
    global usw 
    global user2song
    global users
    global songs
    global userIdx
    global songIdx
    global userCount
    global songCount
    global currentSong
    userId = int(l[0])
    songId = int(l[1])
    weight = float(l[2])
    #ADD
    #users.add(userId)
    #songs.add(songId)
    if userId not in userIdx:
        userIdx[userId] = userCount    
        userCount = userCount + 1
    if songId not in songIdx:
        songIdx[songId] = songCount
        songCount = songCount + 1
    if songId not in currentSong:
        currentSong[songId] = (0,0)
    (c,w) = currentSong[songId]
    if (songId == 18351):
        print("DA")
    currentSong[songId] = (c+1,w+weight)
    user2song[(userId,songId)] = weight
    usw.append((userId,songId,weight))

def readTrain():    
    f = open("user_artists_training.dat", "rt", encoding="utf8")
    next(f)
    for l in csv.reader(f):
        parseLineTrain(l[0].split('\t'))

def parseTest(l):
    global songCount
    global userCount
    global queries
    userId = int(l[0])
    songId = int(l[1])     
    if userId not in userIdx:
        userIdx[userId] = userCount    
        userCount = userCount + 1
    if songId not in songIdx:
        songIdx[songId] = songCount
        songCount = songCount + 1    
    queries.append((userId,songId))
   
def readTest():    
    f = open("user_artists_test.dat", "rt", encoding="utf8")
    next(f)
    for l in csv.reader(f):
        parseTest(l[0].split('\t'))

def init():
    readTrain()
    readTest()
    #print("unique users:"+str(userCount))
    #print("unique songs:"+str(songCount))
    #print("unique pairs:"+str(len(usw)))

def solve(tmpUsw,k=50,a=0.01,n = 0.02):
    #global usw
    global userCount
    global songCount
    global currentSong
    global queries
    lim = 1e-2
    np.random.seed(599)
    P = np.random.uniform(-lim,lim,(userCount,k+1))
    Q = np.random.uniform(-lim,lim,(k+1,songCount))
    P[:,0] = 1
    Q[k,:] = 1
    iter_ = 14
    prev_e = np.inf
    while iter_ >= 0:
        for (u,i,r) in tmpUsw:
            u = userIdx[u]
            i = songIdx[i]
            p_u = P[u,:].copy()
            q_i = Q[:,i].copy()
            e = np.sqrt(0.5)*(r - np.dot(p_u,q_i))
            p_u = p_u + a*(e*Q[:,i]-n*P[u,:])
            q_i = q_i + a*(e*P[u,:]-n*Q[:,i])
            P[u,:] = p_u
            Q[:,i] = q_i            
            P[:,0] = 1
            Q[k,:] = 1
        iter_ = iter_ - 1
    print(iter_)

    printSol(P,Q)

def printSol(P,Q):
    global user2song
    xx = 0
    with open('solved.txt', 'w') as fw:
        for (u,s) in queries:
            if (u,s) in user2song:
                print(user2song[(userIdx[u],songIdx[s])])
            else:
                pred = 1.82106569656
                
                pred = np.dot(P[userIdx[u],:],Q[:,songIdx[s]])
                pred = min(pred,10)
                if pred < 0 and not s in currentSong:
                    pred = 0
                else:
                    if pred < 0 and s in currentSong:
                        (c,w) = currentSong[s]  
                        pred = w/c
                    elif pred > 10:
                        pred = 10
                    else:
                        xx += 1
                fw.write("%.11f\n" % pred)
def MAE(calculated, expected):
    if len(calculated)!=len(expected) or len(calculated) == 0 or len(expected) == 0:
        return 0
    return np.sum(np.abs(np.subtract(calculated,expected)))/len(calculated)
def RMSE(calculated, expected):
    return np.sqrt(MSE(calculated, expected))
def MSE(calculated, expected):
    if len(calculated)!=len(expected) or len(calculated) == 0 or len(expected) == 0:
        return 0
    return np.sum(np.square(np.subtract(calculated,expected)))/len(calculated)

def test_val(k=50,a=0.01,n = 0.017):
    global usw
    #np.random.seed(547)
    np.random.seed(599)
    train_len = int(len(usw)*0.7)
    test_len = len(usw) - train_len
    shuffle = np.random.permutation(len(usw))
    shuffle = list(shuffle)
    lim = 1e-2
    P = np.random.uniform(-lim,lim,(userCount,k+1))
    Q = np.random.uniform(-lim,lim,(k+1,songCount))
    P[:,0] = 1
    Q[k,:] = 1
    iter_ = 11
    prev_best = np.inf
    while iter_ >= 0:    
        for idx in range(train_len):
                idx = shuffle[idx]
                (u,i,r) = usw[idx]
                u = userIdx[u]
                i = songIdx[i]
                p_u = P[u,:].copy()
                q_i = Q[:,i].copy()
                e = (r - np.dot(p_u, q_i))
                p_u = p_u + a*(e*Q[:,i]-n*P[u,:])
                q_i = q_i + a*(e*P[u,:]-n*Q[:,i])
                P[u,:] = p_u
                Q[:,i] = q_i    
                P[:,0] = 1
                Q[k,:] = 1    
        RMSE = 0
        for idx in range(test_len):
            idx = idx + train_len
            idx = shuffle[idx]
            (u,i,r) = usw[idx]
            pred = np.dot(P[userIdx[u],:], Q[:,songIdx[i]])
            RMSE = RMSE + (r-pred)*(r-pred)
        if(prev_best > RMSE):
            prev_best = RMSE
        else:
            break
        print(np.sqrt(prev_best / test_len))
        iter_ = iter_ - 1
    print(iter_)
    return np.sqrt(prev_best / test_len),iter_
def findK():
    solve(usw,50,0.01,0.01)
if __name__ == "__main__":
    init()
    global_RMSE = np.inf
    bestSeed  = 0
    iter0 = 0
    
    
    # print(usw[:10])
    
    solve(usw,30,0.01,0.01)

    #solve(usw,50,0.01,0.02)
   # import math
    #for num in range(1001,1600,2):
    #    if all(num%i!=0 for i in range(3,int(math.sqrt(num))+1, 2)):
    #        np.random.seed(num)
    #val,iter_ = test_val(51,0.01,0.017)
    #        if(val < global_RMSE):
    #            global_RMSE = val
     #           bestSeed = num
     #           iter0 = iter_
    #        print("SEED = "+str(num)+" RMSE: "+str(global_RMSE))  


  
    #global usw
    #z = np.array(usw)
    #for x in z:
     #   print(x)
    #solve(usw,51,0.01,0.0168867)
    #for k in range(30,60):
    #val = test_val(50,0.01,0.017)
    #    n = n + 0.001
    #    print("n = " + str(n%0.1)+ " " + str(val))
    #print(userCount)
