import numpy as np
from scipy.optimize import fmin_l_bfgs_b


def load(name):
    """ 
    Odpri datoteko. Vrni matriko primerov (stolpci so znacilke) 
    in vektor razredov.
    """
    data = np.loadtxt(name)
    X, y = data[:, :-1], data[:, -1].astype(np.int)
    return X, y

""" 
    Napovej verjetnost za razred 1 glede na podan primer (vektor vrednosti
    znacilk) in vektor napovednih koeficientov theta.
"""
def h(x, theta):
    return 1/(1+np.exp(-(theta.T.dot(x))))
"""
    NOT HC:
    n, m = X.shape
    ln = np.arange(n)
    return sum(-y*np.log(h(X[ln].T,theta))-(1-y)*np.log(1-h(X[ln].T,theta)))/n+lambda_*(theta.T.dot(theta))
    Vrednost cenilne funkcije.
"""
def cost(theta, X, y, lambda_):
    return sum(-y*np.log(h(X[np.arange(X.shape[0])].T,theta)+np.finfo(float).eps)-(1-y)*np.log(1-h(X[np.arange(X.shape[0])].T,theta)+np.finfo(float).eps))/X.shape[0]+lambda_*(theta.T.dot(theta))/X.shape[0]
"""
    Odvod cenilne funkcije. Vrne 1D numpy array v velikosti vektorja theta.
"""
def grad(theta, X, y, lambda_):
    return -(y-h(X[np.arange(X.shape[0])].T,theta)).dot(X)/X.shape[0] + lambda_*2*theta/X.shape[0]

def num_grad(theta, X, y, lambda_):
    eps=1e-3
    m,n = X.shape
    theta = np.tile(theta,(n,1))
    dx = np.diag(np.ones((n))*eps)
    theta_left = theta + dx
    theta_right = theta - dx
    fun_val_left = np.apply_along_axis(cost,1,theta_left,X,y,lambda_)
    fun_val_right = np.apply_along_axis(cost,1,theta_right,X,y,lambda_)
    return (fun_val_left - fun_val_right)/(2*eps)


class LogRegClassifier(object):

    def __init__(self, th):
        self.th = th

    def __call__(self, x):
        """
        Napovej razred za vektor vrednosti znacilk. Vrni
        seznam [ verjetnost_razreda_0, verjetnost_razreda_1 ].
        """
        x = np.hstack(([1.], x))
        p1 = h(x, self.th)  # verjetno razreda 1
        return [1-p1, p1]


class LogRegLearner(object):

    def __init__(self, lambda_=0.0):
        self.lambda_ = lambda_

    def __call__(self, X, y):
        """
        Zgradi napovedni model za ucne podatke X z razredi y.
        """
        X = np.hstack((np.ones((len(X),1)), X))

        # optimizacija
        theta = fmin_l_bfgs_b(
            cost,
            x0=np.zeros(X.shape[1]),
            args=(X, y, self.lambda_),
            fprime=grad)[0]

        return LogRegClassifier(theta)


def test_learning(learner, X, y):
    """ vrne napovedi za iste primere, kot so bili uporabljeni pri učenju.
    To je napačen način ocenjevanja uspešnosti!

    Primer klica:
        res = test_learning(LogRegLearner(lambda_=0.0), X, y)
    """
    c = learner(X,y)
    results = [c(x) for x in X]
    return results


def test_cv(learner, X, y, k=5):
    m, n = X.shape
    shuffle = np.random.permutation(m)
    folds = np.array_split(shuffle,k)
    out = np.zeros((m,2))
    iter_ = 0;
    for i in range(k):
        idxs = list(set(shuffle) - set(folds[i]))
        trainX = X[idxs,:]
        trainY = y[idxs]
        testX = X[folds[i],:]
        classify = learner(trainX,trainY)
        for j in range(len(folds[i])):
            out[shuffle[iter_]] = classify(testX[j])
            iter_ = iter_ + 1
    return out

def CA(real, predictions):
   return (((np.array(predictions)[:,0]>np.array(predictions)[:,1]) + real)%2).sum()/real.shape[0]

def AUC(real, predictions):
    pass

def test_score():
    X, y = load('reg.data')
    starat_lambda = 1
    low_lambda = 0.00000000001
    while starat_lambda > low_lambda:
        learner = LogRegLearner(starat_lambda)
        print("Lambda:", starat_lambda)
        #With CV ( Penalized VERSION by crossvalidation)
        print("Tocnost CV:", CA(y, test_cv(learner, X, y)))
        #Vanilla = without CV ( CLEAN VERSION )
        print("Tocnost VANILLA:", CA(y, test_learning(learner, X, y)))
        starat_lambda = starat_lambda / 10

if __name__ == "__main__":
    # Primer uporabe
    X, y = load('reg.data')
    learner = LogRegLearner(lambda_=0.0)
    print("Tocnost:", CA(y, test_cv(learner, X, y)))
    print("Tocnost:", CA(y, test_learning(learner, X, y)))
 