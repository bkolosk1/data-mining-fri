혼밥 먹고 온라인 게임하고… 30대 남성 절반이상이 비만
박성민 기자입력 2019-10-28 03:00:00 수정 2019-10-28 11:16:45

prevnext

댓글보기0|폰트확대축소|뉴스듣기여성남성|프린트이메일
기사공유 | 
페이스북트위터
빨간불 켜진 국민건강

유통회사에 다니는 미혼 남성 권모 씨(36)는 대학 졸업 당시 68kg이던 몸무게가 10년 만에 91kg으로 늘었다. 회식이나 야근 후 폭식을 하고 일주일에 서너 번씩 배달음식을 먹은 것이 화근이었다. 주말이면 밀린 잠을 자느라 좀처럼 집 밖에 나가지 않는다. 7, 8시간씩 의자에 앉아 온라인 게임에 몰두하는 게 유일한 취미다. 권 씨는 “쇼핑도 주로 온라인으로 하니까 출퇴근 외에는 걷는 시간이 거의 없다”고 말했다.

30대 한국 남성의 건강에 빨간불이 커졌다. 27일 보건복지부와 질병관리본부가 발표한 ‘2018년 국민건강영양조사’에 따르면 지난해 30대 남성의 비만율은 51.4%로 전년의 46.7%보다 4.7%포인트 급증했다. 40대 남성 비만율도 44.7%에서 47.5%로 올랐다. 비만율은 몸무게(kg)를 키(m)의 제곱으로 나눈 체질량지수(BMI)가 25 이상인 사람의 비율이다. 국민건강영양조사는 1998년 시작됐다. 매년 약 1만 명을 대상으로 흡연 음주 영양 등 500여 개 건강지표를 산출하는 최대 규모의 건강통계 조사다.


○ 배달음식 선호, 운동은 뒷전


지난 20년간 한국 남성의 건강관리 성적은 낙제점에 가깝다. 1998년 25.1%였던 19세 이상 성인 남성의 비만율은 지난해 역대 최고인 42.8%로 치솟았다. 반면 같은 기간 여성 비만율은 26.2%에서 25.5%로 낮아졌다.

30대 남성이 살찌는 이유는 더 먹고 덜 움직여서다. 30대는 10대 때부터 햄버거와 피자 같은 고칼로리 패스트푸드에 익숙해졌다. 이번 조사에서 30대 남성의 하루 평균 지방 섭취량은 67.9g으로 1998년 첫 조사 때보다 17.4g이나 늘었다.

이 같은 식습관은 늦은 결혼, ‘혼밥문화’ 확산과 맞물리며 비만율을 끌어올렸다. 2017년 동국대 일산병원 연구진에 따르면 하루 2끼 이상 혼자 식사하는 남성의 복부비만 위험은 그러지 않은 남성보다 45% 높았다. 배달음식이나 편의점 도시락 등은 칼로리가 높은 데다 식사시간도 짧아 살이 찌기 쉽다.

운동보다 게임에 빠진 취미생활의 변화도 비만을 부추겼다. 유산소 운동을 일정 시간 이상 하는 남성 비율은 2015년 62%에서 지난해 51%로 급감했다. 올 5월 한국갤럽 설문조사에서 10∼30대 남성이 가장 좋아하는 취미 1위는 게임이었다.

전문가들은 앞으로가 더 심각하다고 걱정한다. 2017년 기준 남자 아동·청소년 비만율은 26%로 경제협력개발기구(OECD) 회원국 평균 25.6%를 웃돌았다. 이번 조사에서도 주 3회 이상 패스트푸드를 즐기는 청소년 비율은 2009년 12.1%에서 올해 25.5%로 급증했다. 향후 비만 인구가 급증할 가능성이 있다. 국민건강보험공단에 따르면 비만으로 인한 사회적 비용은 2016년 11조4679억 원으로 10년 만에 2.4배로 늘었다.

미국의 경우 2001년 비만을 신종 감염병으로 분류하는 등 비만과의 전쟁을 선포하고 적극적으로 대응하고 있다. 오상우 동국대 일산병원 가정의학과 교수는 “정부가 금연, 치매 관리 등에 주력하면서 비만 관리에 소홀한 측면이 있다”며 “아동부터 청년층의 비만을 철저히 관리해야 만성질환 발생과 사망률을 낮추고 건강보험 재정 악화도 막을 수 있다”고 강조했다.


○ 여성 폭음·흡연, 청소년 우울감도 심각


지난해 성인 여성의 월간 폭음률은 26.9%로 2005년 17.2%에서 급증했다. 역대 최고치였다. 성인 여성 10명 중 3명은 한 달 1회 이상 술 5잔(맥주 3캔) 이상씩 마신다는 뜻이다. 여성의 경제활동 증가로 일과 가정에 모두 충실해야 한다는 스트레스가 커진 것이 음주를 부추긴다는 분석이다.

여성 흡연율도 7.5%로 전년 대비 1.5%포인트 올랐다. 반면 지난해 전체 남성의 흡연율은 36.7%로 역대 가장 낮았다.

청소년의 정신건강도 나빠졌다. 최근 1년간 2주 내내 일상생활을 중단할 만큼 슬픔이나 절망감을 느낀 ‘우울감 경험률’은 2015년 23.6%에서 올해 28.2%로 늘었다. 남학생(22.2%)보다 여학생(34.6%)의 정신건강이 더 취약한 것으로 나타났다.

박성민 기자 


부동산값 급등 지역가입자, 건보료 내달부터 오를듯
전주영 기자입력 2019-10-29 03:00:00 수정 2019-10-29 03:00:00

prevnext

댓글보기0|폰트확대축소|뉴스듣기여성남성|프린트이메일
기사공유 | 
페이스북트위터
공시가격 상승분 재산에 반영, 60등급으로 나눠 건보료 산정
등급 유지땐 보험료 변화 없어

올해 아파트 같은 부동산 가격이 올라 재산이 증가한 건강보험 지역가입자의 보험료가 다음 달부터 오를 것으로 보인다.

국민건강보험공단은 지난해 소득과 올해 재산 변동 사항을 지역가입가구 건강보험료에 반영해 11월분부터 부과한다고 28일 밝혔다. 앞서 국토교통부와 기획재정부 보건복지부 행정안전부 등은 올 1월 표준 단독주택을 시작으로 2월 토지, 4월 아파트 등의 부동산 공시가격을 현실화해 발표했다.

건보공단에 따르면 다음 달 지역가입자 건보료에는 지난해 소득증가율(이자, 배당, 사업, 근로소득 등)과 올해 재산과세표준액(건물, 주택, 토지 등)의 증가가 반영된다.

직장가입자 건보료는 월급과 종합소득에만 부과하지만 지역가입자는 소득, 재산 그리고 자동차에 부과하는 점수를 합산해 건보료를 매긴다. 지역가입자 건보료는 공시가격의 60%를 과세표준액으로 잡는 주택 재산 등을 지역 구분 없이 60등급으로 나눈 ‘재산 보험료 등급표’에 따라 산출한다. 최저 1등급은 450만 원 이하, 최고 60등급은 77억8124만 원 초과다.

소유하고 있는 고가의 아파트 공시가격이 오른 지역가입자는 건보료가 인상될 확률이 높다. 다만 부동산 공시가격이 올랐다고 바로 지역가입자 보험료가 인상되는 것은 아니다. 예를 들어 서울의 시세 6억5500만 원짜리 아파트가 있는 A 씨는 공시가격이 3억7800만 원에서 3억9100만 원으로 올라 재산 보험료 등급표상 30등급에서 31등급이 되면서 건보료가 인상된다. 하지만 경남에서 시세 4억6000만 원 아파트를 소유한 B 씨는 공시가격이 3억2300만 원에서 2억9800만 원으로 줄면서 29등급에서 28등급으로 내려가 건보료가 준다.

11월분 보험료는 12월 10일까지 내야 한다. 소득이 줄었거나 재산을 매각한 경우에는 퇴직·해촉증명서, 소득금액증명원, 등기부등본 등을 준비해 가까운 공단지사에 건보료 조정신청을 할 수 있다.

전주영 기자

