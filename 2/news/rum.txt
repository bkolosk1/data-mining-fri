Cum se împarte țara în secret. Haos total pe străzi. DIICOT încearcă să restabilească ordinea
Autor: Iulia Moise30 octombrie 2019
O anchetă de proporții a DIICOT Timiș, scoate la iveală detalii halucinante despre cum clanurile interlope își împart țara.


STIRI CALDE
20:00 - Unde pot fi folosite trotinetele electrice. Decizie de ultima oră luată de autorități
19:52 - Detalii din dosarul lui Mazăre: „Au funcţionat ca o veritabilă organizaţie de tip mafiot"
19:45 - Record de transplanturi. 18 vieți salvate numai în ultimele două săptămâni
19:37 - Contrabandă cu parfumuri, de 500.000 de euro, în portul Constanța
19:30 - Cum se împarte țara în secret. Haos total. DIICOT încearcă să restabilească ordinea
19:22 - Declarații cutremurătoare. Mama pădurarului ucis în Maramureș: Aici e mare mafie
19:14 - Antena 1 s-a predat! Toți telespectatorii trebuie să știe. Mutare forțată în media
19:07 - Cazul Caracal. Au găsit locul în care se află Alexandra? Mărturie șocantă a groparului
19:00 - Încep să cadă capete în PSD. Acuzații incendiare după excludere. „Nu susțin candidatura”
18:54 - Confruntare între polițiști și traficanții de droguri. S-au tras focuri de armă în Jilava!
18:47 - Date alarmante de la DNA. România, campioană la fraude cu bani europeni

 


 

Anchetatorii susțin că interlopii acționează după modelul mafiei italiene: se împart zone de influență, persoane sunt intimidate, amenințate și sechestrate, se plătesc taxe de protecție și se creează legături cu oameni ai legii.

Cu toate astea, în timp ce DIICOT se străduia să adune probe despre afacerile cu cocaină coordinate de Lucian Boncu, în luna septembrie, liderii interlopi s-au adunat la un congres pentru a stabili taxele de protecție.

La întâlnire au participat lideri interlopi din orașele-cheie ale țării, între care Costel Corduneanu, liderul clanului Corduneanu din Iași, Adrian Tâmplaru, liderul lumii interlope din Sibiu, Alin Iovănut, unul din liderii lumii interlope din Arad și alții. Timișoara a fost reprezentată de Lucian Boncu.


 

Lucian Boncu a spus în gura mare că el deține contolul total la Timișoara și că poate prelua puterea în orice moment., Boncu le-ar fi spus celorlalți interlopi: „Îţi dau un Kalashnikov, mă duc la sârbi şi iau. 50 de proşti pune Timişoara pe burtă, cu tot cu poliţie şi SRI. Omori 50-100 de oameni şi nu mai iese nimeni din casă. În momentul ăla, noi am pus stăpânire pe oraş”.

Boncu și alți 33 de indivizi sunt cercetați de DIICOT – Serviciul Teritorial Timișoara, pentru constituire a unui grup infracțional organizat, lipsire de libertate în mod ilegal şi șantaj.

Potrivit anchetatorilor, Boncu a constituit în Timișoara un „grup infracţional organizat, care în mod coordonat, prin săvârşirea de fapte cu violenţă, a acţionat pentru protejarea intereselor patrimoniale şi nepatrimoniale ale membrilor, precum şi a intereselor diferitor persoane din anturajul acestora, care solicitau intervenţia membrilor grupării pentru rezolvarea pe cale ilicită a unor diferende de natură patrimonială, pentru recuperări de bunuri și valori, pentru reatragerea unor plângeri penale etc. Membrii grupului au acţionat constant cu violenţă pentru câştigarea şi menţinerea sferei de influenţă, cât şi pentru a induce în opinia publică ideea că sunt un grup solidar, care acţionează ferm pentru protejarea propriilor interese.”


 
Procurorii DIICOT au aflat că acoperirea pentru toate activitățile ilegale era o firmă de protecţie şi gardă, înfiinţată de Boncu și preluată formal de altă persoană.

„Profitând de obiectul de activitate al societăţii şi pretextând că intervin la solicitarea clienţilor, grupul infracţional organizat, condus de către suspectul B. L. A., a acţionat în mod repetat, săvârşind fapte cu violenţă asupra mai multor persoane”, potrivit comunicatului DIICOT.

În toate situațiile constatate de-a lungul timpului, ca urmare a presiunilor exercitate asupra părților vătămate, acestea fie nu au depus plângeri penale, fie și le-au retras ulterior. Bărbat sechestrat, bătut, dezbrăcat și obligat să „piardă” la cazinou banii pe care îi câștigase


 

„În acest context, la data de 09.09.2019, o persoană vătămată s-a deplasat la o agenţie, cu punct de lucru situat în municipiul Timişoara, pentru a participa la jocurile de noroc din localurile de profil. În localul amintit, persoana vătămată a jucat la jocurile mecanice şi a câştigat suma de 80.000 de lei, din care a primit suma de 65.000 de lei. Pretextând că nu au bani lichizi, reprezentanţii localului i-au solicitat părții vătămate să revină luni 16.09.2019, pentru plata diferenţei de 15.000 de lei.

Revenind în data respectivă, administratorul localului a refuzat să-i remită persoanei vătămate suma de bani și, pentru a se asigura că persoana vătămată nu pleacă sau sesizează organele de poliţie pentru neplata câştigului, a apelat la suspectul B. L. A, care s-a deplasat la local, împreună cu trei suspecţi și alți doi agenţi de securitate ai societăţii de pază. Suspecții au sechestrat-o apoi pe partea vătămată timp de mai multe ore şi au supus-o violenţelor, aplicându-i lovituri la nivelul capului şi trunchiului, pentru a o determina să joace la aparate pentru a restitui din banii primiți. Pe lângă violenţe, victima a fost obligată la un moment dat să se dezbrace de haine, de la brâu în jos, ameninţată fiind cu supunerea la perversiuni sexuale anale şi orale”, potrivit comunicatului DIICOT.