import os
from unidecode import unidecode
import matplotlib.pyplot as plt
from statistics import mean 
from hc import HierarchicalClustering
import math
import random
class KMeans:
    def __init__(self,based=True):
        self.name_map = {'rum':'Romanian','itn':'Italian','ice':'Icelandic ','nrn':'Norwegian','eng' : 'English','frn' : 'French','ger' : 'German',
        'kkn':'Korean','chn' : 'Chinese','jpn' : 'Japanese','mkj' : 'Macedonian','por' : 'Portuguese','pql' : 'Polish',
            'rus' : 'Russian','czc':'Czech','slv' : 'Slovenian','spn' : 'Spanish','src1' : 'Bosnian','src3' : 'Serbian','swd' : 'Swedish'}
        """
            Init globals.
        """
        self.fileNames = []
        self.lang_triplets = {}
        self.dist_matrix = {}
        self.name_list = list(self.name_map)
        self.name_idx = {}
        self.idx_name = {}
        self.init(based)
        self.best = -1
        self.bestClust = []
        self.bestSil = []
        self.min = 1000000
        self.worstSil = []
        self.worstClust = []

    """ 
        Init states.
    """
    def init(self,based):
        cnt = 0
        for x in self.name_list:
            self.dist_matrix[x] = [0] * len(self.name_map) 
            self.name_idx[x] = cnt
            self.idx_name[cnt] = x
            self.lang_triplets[x] = {}
            cnt = cnt + 1
        if based:
            self.open_files()
        else:
            self.open_files_news_papers()
        self.build_distance_matrix()
    def open_files(self):
        for x in self.name_map.keys():
            path = ".\\ready\\"+x+".txt"
            f = unidecode(open(path , "rt", encoding="utf8").read()).lower().split()
            self.parse_files(x,f)
    def open_files_news_papers(self):
        for x in self.name_map.keys():
            path = ".\\news\\"+x+".txt"
            f = unidecode(open(path , "rt", encoding="utf8").read()).lower().split()
            self.parse_files(x,f)
    """
        Generates tuples, of length k.
    """
    def tuples_generating(self,file_idx,text, k):
        for i in range(len(text)):
            tmp_str = text[i : i + k]
            if not(tmp_str in self.lang_triplets[file_idx]):
                self.lang_triplets[file_idx][tmp_str] = 0
            self.lang_triplets[file_idx][tmp_str] = self.lang_triplets[file_idx][tmp_str] + 1

    def parse_files(self,file_idx, words):
        for word in words:
            word = word.split('\n')
            for part in word:
                self.tuples_generating(file_idx,part,3)

    """
        Calculates vector length as euclidian sum of coords
    """
    def vec_dist(self,l_x):
        len_x = 0
        for x in l_x.values():
            len_x = len_x + x*x
        return math.sqrt(len_x)

    """
        Cosine similarity calculation. For x = y -> 0 else dot(x,y)/(|x||y|)
    """
    def cosine_dist(self,x,y):
        dist = 0
        if(x == y):
            return dist
        s_x_y = (self.lang_triplets[x].keys() & self.lang_triplets[y].keys())
        for i in s_x_y:
            dist = dist + self.lang_triplets[x][i]*self.lang_triplets[y][i]
        x_norm = self.vec_dist(self.lang_triplets[x])
        y_norm = self.vec_dist(self.lang_triplets[y])
        dist = dist/(x_norm*y_norm)
        return 1-dist

    """
        For every entry builds distance matrix based on cosine similarity.
    """
    def build_distance_matrix(self):
        for x in self.name_list:
            for y in self.name_list:
                self.dist_matrix[x][self.name_idx[y]] = self.cosine_dist(x,y)
    """
        Finds closes 
    """
    def find_closest(self,centroids, from_x):
        min_dist = 9999999777
        min_idx = -1
        for x in centroids:
            if(self.dist_matrix[from_x][x] <= min_dist):
                min_dist = self.dist_matrix[from_x][x] 
                min_idx = x
        return min_idx
    """
        Finds which distance to update for current cluster
    """
    def update_current(self,list_centroids):
        avg_beg = 9999999
        idx = "-1"
        for x in list_centroids:
            avg_tmp = 0
            for y in list_centroids:
                avg_tmp = avg_tmp + self.dist_matrix[x][self.name_idx[y]]
            if avg_tmp < avg_beg:
                avg_beg = avg_tmp
                idx = x
            avg_tmp = 0
        return self.name_idx[idx]
    """
        Reassigns centroids to clusters
    """
    def update_centroids(self,centers,centroids):
        len_centers = len(centers)
        tmp_centers = [0] * len_centers
        ptr_centers = 0
        updated = False
        #Copy it
        for ptr in range(len_centers):
            tmp_centers[ptr] = centers[ptr]
        while ptr_centers < len_centers:
            new_center = self.update_current(centroids[centers[ptr_centers]])
            if new_center != centers[ptr_centers]:
                updated = True
                tmp_centers[ptr_centers] = new_center
            ptr_centers = ptr_centers + 1
        return updated,tmp_centers
    """
        a_i of silhouette
    """
    def find_a_i(self,from_cnt, centroids):
        len_cent = len(centroids)
        if len_cent == 1:
            return 1
        avg  = 0 
        for x in centroids:
            avg = avg + self.dist_matrix[from_cnt][self.name_idx[x]]
        return avg/(len_cent-1)
    """
        b_i of silhouette
    """
    def find_b_i(self,y,x,centroids):
        mean_dist_other = 9999999999
        for c in centroids:
            if c != x:
                mean_dist_other = min(mean_dist_other,self.find_a_i(y,centroids[c]))
        return mean_dist_other

    def flatten_cluster(self, centroids):
        results = []
        line = ""
        for x in centroids:
            line = "CENTER: " + self.name_map[self.idx_name[x]] + " -> "
            for y in centroids[x]:
                line = line + " " + self.name_map[y]
            results.append(line)
        return results
    """
        Silhouette formula implemented from wikipedia -> https://en.wikipedia.org/wiki/Silhouette_(clustering)
    """
    def silhouette(self,centroids):
        cnt_silh = [0]*len(self.name_map)
        for x in centroids:
            for y in centroids[x]:
                len_cent = len(centroids[x])
                if len_cent > 1:
                    a_i = self.find_a_i(y,centroids[x])
                    b_i = self.find_b_i(y,x,centroids)
                    cnt_silh[self.name_idx[y]] = (b_i-a_i)/max(a_i,b_i)
                else:
                    cnt_silh[self.name_idx[y]] = 0
        avg = sum(cnt_silh)
        if avg > self.best:
            self.best = avg
            self.bestClust = self.flatten_cluster(centroids)
            self.bestSil = cnt_silh.copy()
        if avg < self.min:
            self.min = avg
            self.worstClust = self.flatten_cluster(centroids)
            self.worstSil = cnt_silh.copy()
        return avg/len(self.name_map)
                
    def print_centers(self,centeroids):
        for c in centeroids:
            print(self.name_map[self.idx_name[c]])
    
    def print_dist_matrix(self):
        print("    ",end='')
        for c in self.dist_matrix:
            print("%5s " % c,end='')
        print()
        for c in self.dist_matrix:
            print("%5s " % c,end='')
            for y in self.dist_matrix[c]:
                print("%.3f " % y,end='')
            print()
    """
        Exports distances to .dst file
    """
    def export_distance(self):
        f = open("nal.dist","w+")
        it = 1
        f.write("axis = %d"  % (len(self.name_list)))  
        for i in self.dist_matrix:
            for j in range(it):               
               f.write("%f " % (self.dist_matrix[i][j]))
            it = it + 1
            f.write("\n")
        f.close()

    def print_clusters(self,centroids):
        print("**************************************")
        print(centroids)
        for x in centroids:
            print("CENTER: "+self.name_map[self.idx_name[x]])
            for y in centroids[x]:
                print(self.name_map[y])

    def k_menas_clustering(self,k,_print=True):
        N = len(self.name_idx)
        centroids = {}
        centers = set()
        #Generating random idx
        while len(centers) < k:
            tmpIdx = random.randint(0, N-1)
            centers.add(tmpIdx)           
        for x in centers:  
            centroids[x] = []
        centers = list(centers)
        #print("INITIAL CENTERS")
        #print_centers(centroids)
        iterations = 1
        while True:   
            updated = False 
            #Find closes medoid for x
            for x in self.name_list:
                ptr_centroid = self.find_closest(centers,x)
                centroids[ptr_centroid].append(x)
            updated,new_centers = self.update_centroids(centers,centroids)
            #print("UPDATED CENTERS")
            #print_centers(centroids)
            if (updated):
                centroids.clear()
                for c in new_centers:
                    centroids[c] = []
                centers = new_centers
            else:
                break
            iterations = iterations + 1
        if _print:
            self.print_clusters(centroids)
        return self.silhouette(centroids)    
    """
        Helper function for third task, lazy implementation.
    """
    def cast_cosine_dist(self,x,y):
        dist = 0
        if(x == y):
            return dist
        s_x_y = (self.lang_triplets[x].keys() & y.keys())
        for i in s_x_y:
            dist = dist + self.lang_triplets[x][i]*y[i]
        x_norm = self.vec_dist(self.lang_triplets[x])
        y_norm = self.vec_dist(y)
        dist = dist/(x_norm*y_norm)
        return dist
    """
        Helper function for third task, lazy implementation.
    """
    def cast_tuples_generating(self,text,new_cast_text,k):
        for i in range(len(text)):
            tmp_str = text[i : i + k]
            if not(tmp_str in new_cast_text):
                new_cast_text[tmp_str] = 0
            new_cast_text[tmp_str] = new_cast_text[tmp_str] + 1
        return new_cast_text
    """
        Helper function for third task, lazy implementation.
    """
    def cast_parse_files(self, words):
        new_cast_text = {}
        for word in words:
            word = word.split('\n')
            for part in word:
                new_cast_text = self.cast_tuples_generating(part,new_cast_text,3)
        return new_cast_text
    """
        Third task, lazy implementation.
    """
    def find_matching(self,_id):
        path =".\\news\\"+_id+".txt"
        f = unidecode(open(path , "rt", encoding="utf8").read()).lower().split()
        text_processed = self.cast_parse_files(f)
        arg_max = 0
        cos_max = -11
        cos_sim = [0] * len(self.name_list)
        for x in self.name_list:
            cos_sim[self.name_idx[x]] = self.cast_cosine_dist(x,text_processed)
            if cos_sim[self.name_idx[x]]>cos_max:
                cos_max = cos_sim[self.name_idx[x]]
                arg_max = x
        sum_ = sum(cos_sim)
        print("MAXIMUM MATCHING WITH "+ self.name_map[arg_max])    
        top_3 = sorted(zip(cos_sim,self.name_list),reverse=True)
        sum_ = 0
        for i in range(3):
            sum_ += top_3[i][0]
        for i in range(3):
            x,y = top_3[i]
            print("%15s %.4f" % (self.name_map[y], x/sum_))



#SECOND TASK
def print_best_worst(kmeans):
    silh_res = [0] * 100
    for i in range(100):
       silh_res[i] = kmeans.k_menas_clustering(5,False)      
    print("Best cluster: ")
    for x in kmeans.bestClust: 
        print(x+" ")
    print("*******************")
    print("Worst cluster: ")
    for x in kmeans.worstClust:
        print(x+" ")
    plt.hist(silh_res)
    plt.grid(True)
    plt.show()  

#THIRD TASK
def classification_test(kmeans):
    print("TUKA")
    N = len(kmeans.name_list)
    idx = random.randint(0, N-1)
    print("Prompting text from: "+kmeans.name_map[kmeans.idx_name[idx]])
    print("RESULTS: ")
    kmeans.find_matching(kmeans.idx_name[idx])

#FOURTH TASK
def hc(kmeans):
    hc = HierarchicalClustering(kmeans.dist_matrix,kmeans.name_idx,kmeans.name_map)
    hc.run()
    hc.plot_tree()

#FIFTH TASK
def news_test():
    kmeans_news = KMeans(False)
    for i in range(100):
       kmeans_news.k_menas_clustering(5,False)  
    print("Clustering on news results: ")
    print("Best cluster: ")
    for x in kmeans_news.bestClust: 
        print(x+" ")
    print("*******************")
    print("Worst cluster: ")
    for x in kmeans_news.worstClust:
        print(x+" ")  


    
if __name__ == "__main__":
    kmeans =  KMeans(True)
    #FIRST TASK
    kmeans.print_dist_matrix()
    #SECOND TASK 
    print_best_worst(kmeans)
    #THIRD TASK
    classification_test(kmeans)
    #FOURTH TASK
    hc(kmeans)
    #FIFTH TASK
    news_test()